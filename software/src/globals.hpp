#pragma once

#ifndef __globals_hpp_included
#define __globals_hpp_included

typedef unsigned char uchr;

// Simple unless() ( == if(!) ) macro
#define HORIZON_UNLESS(x)   if(!(x))

// But no, seriously. I was programming in Ruby for the past two months, not having an unless construct feels like not having a hand.

#endif